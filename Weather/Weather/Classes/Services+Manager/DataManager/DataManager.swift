//
//  DataManager.swift
//  Weather
//
//  Created by Mac on 10/8/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import Foundation
import MapKit

class DataManager {
    
    var annotationsArray : Array<Annotation>?

    internal class var sharedInstance: DataManager {
        struct Singleton {
            static let instance = DataManager()
        }
        return Singleton.instance
    }
    
    func addAnnotation(annotation: Annotation) -> Void {
        annotationsArray?.append(annotation)
        saveData()
    }
    
    func removeAnnotation(index: Int) -> Void {
        guard index < (annotationsArray?.count)! else {
            return
        }
        annotationsArray?.remove(at: index)
        saveData()
    }
    
    /// Saving lat/Long to DB
    func saveData() -> Void {
        guard annotationsArray != nil && (annotationsArray?.count)! > 0 else {
            return
        }
        
        let finalArray = NSMutableArray()
        for annotation in annotationsArray! {
            let dict = annotation.encodeToDictionaryRepresentation()
            finalArray.add(dict)
        }
        
        let path = dataFilePath()
        let _ = finalArray.write(toFile: path, atomically: true)
    }
    
    //Mark: Private Functions
    private init() {
        initialisingDataManager()
    }
    
    /// Initialising DataManager Annotation to DB
    private func initialisingDataManager() {
        annotationsArray = Array()
        let path = dataFilePath()
        if let savedArray = NSArray(contentsOfFile: path) {
            for dict in savedArray {
                let annotation = Annotation(withDictionary: dict as! NSDictionary)
                addAnnotation(annotation: annotation)
            }
        }
    }
    
    
    /// Document Directory Path for Data PLIST
    ///
    /// - Returns: path String
    private func dataFilePath() -> String {
        return Utils.documentDirectoryPath() + "/data.plist"
    }

}
