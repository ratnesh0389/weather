//
//  ViewController.swift
//  Weather
//
//  Created by Mac on 10/4/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    let cellIdentifier = "HomeCell"
    var filterArray = Array<Annotation>()
    var isSearching = false
    var searchStr : String?
    
    @IBOutlet weak var citiesTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setUpIntials()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        citiesTableView.reloadData()
    }
    
    //MARK:- UI setu methods
    
    func setUpIntials() {
        
        //Setup UI
        self.title = Message.kHomeTitleString
        setupTableView()
    }
    
    //MARK: Private Functions
    /// Setup Table View attributes
    private func setupTableView() -> Void {
        citiesTableView.separatorColor = kPrimaryTextColor
        citiesTableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    //MARK- Button Action
    @IBAction func goToCityScreenAction(_ sender: UIBarButtonItem) {
        let mainStoryBoard = UIStoryboard(name: kMainID, bundle: Bundle.main)
        let cityVC: CityViewController = mainStoryBoard.instantiateViewController(withIdentifier: kCityViewControllerID) as! CityViewController
        
        self.navigationController?.pushViewController(cityVC, animated: true)
    }
    
    @IBAction func addLocationButtonAction(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: kMainID, bundle: Bundle.main)
        let locationVC: LocationViewController = mainStoryBoard.instantiateViewController(withIdentifier: kLocationViewControllerID) as! LocationViewController
        
        self.navigationController?.pushViewController(locationVC, animated: true)
        
    }
    
    @IBAction func goToHelpScreenAction(_ sender: UIBarButtonItem) {
        let mainStoryBoard = UIStoryboard(name: kMainID, bundle: Bundle.main)
        let helpVC: HelpViewController = mainStoryBoard.instantiateViewController(withIdentifier: kHelpViewControllerID) as! HelpViewController
        
        self.navigationController?.pushViewController(helpVC, animated: true)
        
    }
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryBoard = UIStoryboard(name: kMainID, bundle: Bundle.main)
        let cityVC: CityViewController = mainStoryBoard.instantiateViewController(withIdentifier: kCityViewControllerID) as! CityViewController
        
        cityVC.annotation = DataManager.sharedInstance.annotationsArray?[indexPath.row]
        
        self.navigationController?.pushViewController(cityVC, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        DataManager.sharedInstance.removeAnnotation(index: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .left)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let searchBar = UISearchBar.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        return searchBar
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching == true {
            return filterArray.count
        } else {
            return (DataManager.sharedInstance.annotationsArray!.count)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Instantiate a cell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        
        var title = ""
        var subTitle = ""
        if isSearching != true {
            title = ((DataManager.sharedInstance.annotationsArray?[indexPath.row])?.title)!
            subTitle = ((DataManager.sharedInstance.annotationsArray?[indexPath.row])?.subtitle)!
            cell.locationAddress1.text = title
        } else {
            title = (filterArray[indexPath.row]).title!
            subTitle = (filterArray[indexPath.row]).subtitle!
            let range = (title as NSString).range(of: searchStr!, options: .caseInsensitive)
            let attributedString = NSMutableAttributedString(string:title)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: kSecondryTextColor , range: range)
            cell.locationAddress1?.attributedText = attributedString
        }
        
        cell.locationAddress2?.text = subTitle
        //        cell.accessoryType = .disclosureIndicator
        return cell
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        searchStr = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
        searchStr = nil
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchStr = searchText
        let arr = DataManager.sharedInstance.annotationsArray?.filter {
            $0.title?.range(of: searchText, options: .caseInsensitive) != nil
        }
        filterArray.removeAll()
        guard arr != nil else {
            filterArray.append(contentsOf: DataManager.sharedInstance.annotationsArray!)
            citiesTableView.reloadData()
            return
        }
        filterArray.append(contentsOf: arr!)
        citiesTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearching = false
        citiesTableView.reloadData()
    }
}
