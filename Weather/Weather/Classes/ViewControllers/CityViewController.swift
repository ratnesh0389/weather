//
//  CityViewController.swift
//  Weather
//
//  Created by Mac on 10/5/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var minMaxTempLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var elevationLabel: UILabel!
    
    @IBOutlet weak var forecastTableView: UITableView!
    
    var annotation: Annotation?
    
    var weatherModel:WeatherModel?
    var forecastModel:ForecastModel?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpIntials()
    }
    
    func setUpIntials() {
        
        //Setup UI
        self.title = (annotation?.title!)! + " Weather Report"
        self.navigationController?.navigationBar.isTranslucent = false

        getWeatherData()
    }
    
    /// Getting Weather Data
    private func getWeatherData() -> Void {
        var unit = UserDefaults.standard.object(forKey: UserDefaultKeys.TemperaturUnitKey)
        unit = unit == nil ? "Metric" : unit
        guard annotation != nil else {
            return
        }
        fetchWeather(forLatitude: (annotation?.coordinate.latitude)!, Longitude: (annotation?.coordinate.longitude)!, TempratureUnit: unit as! String)
    }

    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    /// Fecthing Weather API
    private func fetchWeather(forLatitude lat: Double, Longitude long: Double, TempratureUnit unit: String) {
        let weatherAPI = WeatherAPI()
        weatherAPI.callback = { respose in
            if respose != nil  {
                self.weatherModel = WeatherModel(dictionary: respose as! [String : AnyObject])
                self.setUpView()
                self.getForecastData()
            } else {
                
            }
        }
        weatherAPI.getWeather(forLatitude: lat, Longitude: long, TempratureUnit: unit)
    }
    
    /// Getting Forecast Data
    private func getForecastData() -> Void {
        var unit = UserDefaults.standard.object(forKey: UserDefaultKeys.TemperaturUnitKey)
        unit = unit == nil ? "Metric" : unit
        guard annotation != nil else {
            return
        }
        fetchForecast(forLatitude: (annotation?.coordinate.latitude)!, Longitude: (annotation?.coordinate.longitude)!, TempratureUnit: unit as! String)
    }
    
    /// Fecthing Forecast API
    private func fetchForecast(forLatitude lat: Double, Longitude long: Double, TempratureUnit unit: String) {
        let forecastAPI = ForecastAPI()
        forecastAPI.callback = { respose in
            if respose != nil  {
                self.forecastModel = ForecastModel(dictionary: respose as! [String : AnyObject])
                self.forecastTableView.reloadData()
            } else {
                
            }
        }
        forecastAPI.getForecast(forLatitude: lat, Longitude: long, TempratureUnit: unit)
    }
    
    
    /// Setting Up View
    private func setUpView(){
        if weatherModel != nil{
            temperatureLabel.text = weatherModel!.temperature!.description
            weatherLabel.text = weatherModel!.weather
            minMaxTempLabel.text = "\(weatherModel!.maxTemp!.description)/\(weatherModel!.minTemp!.description)"
            windLabel.text = weatherModel?.windSpeed?.description
            pressureLabel.text = weatherModel?.pressure?.description
            humidityLabel.text = weatherModel?.humidity?.description
            elevationLabel.text = weatherModel?.elevation?.description
        }
    }
    
    private func showNoDataAlert() {
        let cancleAction = UIAlertAction.init(title: "Move Back", style: .cancel, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
        })
        let message = "Unable to fetch Weather Information, Please try again."
        let title = "Oops!!"
        Utils.showAlert(title: title, message: message, actions: [cancleAction])
    }
    
}

extension CityViewController :UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        guard forecastModel != nil else {
            return 0
        }
        return (forecastModel?.forecastArray.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDetailsTableViewCell") as! HomeDetailsTableViewCell

        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        let result = formatter.string(from: forecastModel!.forecastArray[indexPath.row].date!)
        cell.dateTitleLabel?.text = String(describing: result)
        cell.tempLabel.text = "\(forecastModel!.forecastArray[indexPath.row].maxTemp!)/\(forecastModel!.forecastArray[indexPath.row].minTemp!)"
        
        cell.windLabel.text = "\(forecastModel!.forecastArray[indexPath.row].windSpeed!)"
        cell.rainStatusLabel.text = "\(forecastModel!.forecastArray[indexPath.row].weather!)"
        cell.humidityLabel.text = "\(forecastModel!.forecastArray[indexPath.row].humidity!)"
        
        return cell
    }
}

