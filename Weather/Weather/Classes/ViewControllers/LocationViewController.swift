//
//  LocationViewController.swift
//  Weather
//
//  Created by Mac on 10/5/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setUpIntials()
    }
    
    func setUpIntials() {
        
        //Setup UI
        self.title = Message.kLocationTitleString
        setLongPressGesture()
    }
    
    
    //MARK: Private Functions
    /// Long Press Gesture for Placing pin on MKMap
    private func setLongPressGesture() -> Void {
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotationOnLongPress(gesture:)))
        longPressGesture.minimumPressDuration = 0.5
        self.mapView.addGestureRecognizer(longPressGesture)
    }
    
    /// Long Press Gesture Action
    ///
    /// - Parameter gesture: gesture instance
    @objc private func addAnnotationOnLongPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .ended {
            let point = gesture.location(in: self.mapView)
            let coordinate = self.mapView.convert(point, toCoordinateFrom: self.mapView)
            let pointAnnotation = MKPointAnnotation()
            pointAnnotation.coordinate = coordinate
            
            let loc = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            CLGeocoder().reverseGeocodeLocation(loc, completionHandler: { (placemarks, error) in
                if error != nil {
                    Utils.showAlert(title: "Error", message: (error?.localizedDescription)!, actions: nil)
                } else {
                    let placemark = placemarks?.first
                    if placemark?.administrativeArea != nil && placemark?.country != nil {
                        let annotation = Annotation(withCoordinate: coordinate)
                        annotation.title = placemark?.administrativeArea
                        annotation.subtitle = placemark?.country
                        
                        let cancleAction = UIAlertAction.init(title: "Cancle", style: .cancel, handler: { (action) in
                            self.mapView.removeAnnotation(pointAnnotation)
                        })
                        let confirmAction = UIAlertAction.init(title: "Confirm", style: .default, handler: { (action) in
                            self.mapView.addAnnotation(pointAnnotation)
                            DataManager.sharedInstance.addAnnotation(annotation: annotation)
                        })
                        let message = "Would you like to bookmark this location?"
                        let title = annotation.title! + ", " + annotation.subtitle!
                        Utils.showAlert(title: title, message: message, actions: [cancleAction, confirmAction])
                    } else {
                        Utils.showAlert(title: "Oops!!", message: "Unable to identify location. Please place pin at correct or different location.", actions: nil)
                    }
                }
            })
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
