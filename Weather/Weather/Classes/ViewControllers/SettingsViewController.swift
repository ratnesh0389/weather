//
//  SettingsViewController.swift
//  Weather
//
//  Created by Mac on 10/5/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setUpIntials()
    }
    
    func setUpIntials() {
        
        //Setup UI
        self.title = Message.kSettingsTitleString
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
