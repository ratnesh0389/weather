//
//  HelpViewController.swift
//  Weather
//
//  Created by Mac on 10/5/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import UIKit
import WebKit

class HelpViewController: UIViewController, WKNavigationDelegate {
    
    var webView : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setUpIntials()
    }
    
    func setUpIntials() {
        
        //Setup UI
        self.title = Message.kHelpTitleString
        self.navigationController?.navigationBar.isTranslucent = false
        
        loadHelpScreenWebPage()
    }
    
    
    func loadHelpScreenWebPage(){
        
        guard let pdfUrl = Bundle.main.url(forResource: "MobileAssignmentPS", withExtension: "pdf", subdirectory: nil, localization: nil) else {
            return
        }
        
        // init and load request in webview.
        webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        let request =  URLRequest(url: pdfUrl )
        webView.load(request as URLRequest)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(webView)
        self.view.sendSubviewToBack(webView)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        debugPrint(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        debugPrint("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("finish to load")
    }
    
}
