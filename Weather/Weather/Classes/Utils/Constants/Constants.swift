//
//  Constants.swift
//  Weather
//
//  Created by Mac on 10/5/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import Foundation
import UIKit

// API URLS
enum ApiUrl {
    static let APIKey = "c6e381d8c7ff98f0fee43775817cf6ad"
    
    static let DOMAIN_URL = "https://api.openweathermap.org/"
    
    static let BASE_URL = DOMAIN_URL+"data/2.5"
    
    static let timeOutInterval = 60
}

struct Message {
    
    //String Screen Titles
    static let kHomeTitleString = "Home"
    static let kLocationTitleString = "Pin Your Location"
    static let kCityTitleString = "City"
    static let kHelpTitleString = "Help"
    static let kSettingsTitleString = "Settings"
    
    //String Alert Message/Title.
    static let kOkButtonTitle = "OK"
    static let fatalError = "FATAL ERROR"
    
    static let kNetworkErrorMessgae = "Network error occurred!. Please check your internet and try again."
    static let kAPIParseErrorMessgae = "An error occurred while processing response!. Please check your internet setting and try again later"
}

struct UserDefaultKeys {
    static let TemperaturUnitKey = "TemperaturUnitKey"
}

//Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit
enum TempratureUnitType : Int {
    case Kelvin = 0
    case Celsius = 1
    case Fahrenheit = 2
    internal static func getTempratureUnitKey(forType type: TempratureUnitType) -> String {
        switch type {
        case .Celsius:
            return "Metric"
        case .Fahrenheit:
            return "Imperial"
        case .Kelvin:
            return "Default"
        }
    }
    
    internal static func getTemperatureTypeString(forType type: TempratureUnitType) -> String {
        switch type {
        case .Celsius:
            return "Celsius"
        case .Fahrenheit:
            return "Fahrenheit"
        case .Kelvin:
            return "Kelvin"
        }
    }
    
    internal static func getTemperatureUnit(forType type: TempratureUnitType) -> String {
        switch type {
        case .Celsius:
            return "°C"
        case .Fahrenheit:
            return "°F"
        case .Kelvin:
            return "K"
        }
    }
    
}
// Color constants
let kPrimaryTextColor = UIColor.black
let kSecondryTextColor = UIColor.darkGray
let kPrimaryBackGroundColor = UIColor.lightGray

//Storyboard ID's
let kMainID = "Main"
let kTableViewHeight  = 44.0

let kCityViewControllerID = "CityViewControllerID"
let kLocationViewControllerID = "LocationViewControllerID"
let kHelpViewControllerID = "HelpViewControllerID"
let kSettingsViewControllerID = "SettingsViewControllerID"

