//
//  Utils.swift
//  Weather
//
//  Created by Mac on 10/5/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    internal class func showAlert(title: String, message: String, actions:[UIAlertAction]?) {
        let vc = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let actionArray = actions {
            for action in actionArray {
                alert.addAction(action)
            }
        } else {
            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(okAction)
        }
        vc?.present(alert, animated: true, completion: nil)
    }

    internal static func documentDirectoryPath() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }

    internal class func performWithDelay(seconds: Double, completion:@escaping ()->()) {
        let when = DispatchTime.now() + seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            completion()
        }
    }

    internal class func serializeJSONData(data: Data) -> [String:Any]? {
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data, options:[]) as! [String:Any]
            return jsonResult
        } catch {
            print("ERROR: Unable to Serialize Data to Dict")
        }
        return nil
    }

}
