//
//  HomeTableViewCell.swift
//  Weather
//
//  Created by Mac on 10/9/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet var locationAddress1: UILabel!
    @IBOutlet var locationAddress2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
