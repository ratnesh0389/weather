//
//  HomeDetailsTableViewCell.swift
//  Weather
//
//  Created by Mac on 10/9/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import UIKit

class HomeDetailsTableViewCell: UITableViewCell {

    @IBOutlet var dateTitleLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var rainStatusLabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var windLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
