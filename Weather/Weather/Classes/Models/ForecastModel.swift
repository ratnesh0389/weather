//
//  ForecastModel.swift
//  Weather
//
//  Created by Mac on 10/8/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import Foundation

class ForecastModel {
    var forecastArray = [WeatherForecastModel]()
    init(dictionary:[String:AnyObject]) {
        if let listArray = dictionary["list"] as? [[String:AnyObject]]{
            for item in listArray{
                forecastArray.append(WeatherForecastModel(dictionary: item))
            }
        }
    }
}

class WeatherForecastModel {
    var minTemp: Double?
    var date:Date?
    var maxTemp: Double?
    var temperature: Double?
    var weather: String?
    var windSpeed: Double?
    var humidity:Int?
    var pressure:Double?
    var elevation:Double?

    
    
    init(dictionary:[String:AnyObject]) {

        if let weatherDesc = dictionary["weather"] as? [[String:AnyObject]]{
            if let weatherCond = weatherDesc.first?["description"] as? String{
                weather = weatherCond
            }
        }
        if let main = dictionary["main"] as? [String:AnyObject] {
            if let temp = main["temp"] as? Double {
                temperature = temp
            }
            if let press = main["pressure"] as? Double {
                pressure = press
            }
            if let humid = main["humidity"] as? Int {
                humidity = humid
            }
            if let min = main["temp_min"] as? Double {
                minTemp = min
            }
            
            if let max = main["temp_max"] as? Double {
                maxTemp = max
            }
            if let height = main["sea_level"] as? Double {
                elevation = height
            }
        }
        
        if let wind = dictionary["wind"]?["speed"] as? Double {
            windSpeed = wind
        }

        if let timeStamp = dictionary["dt"] as? Double{
            date =  Date(timeIntervalSince1970: timeStamp)
        }
    }
    
}
