//
//  ForecastAPI.swift
//  Weather
//
//  Created by Mac on 10/9/18.
//  Copyright © 2018 Ratnesh. All rights reserved.
//

import Foundation

class ForecastAPI: HTTPClient {
    required internal override init() {
        super.init()
        self.methodName = "/forecast"
    }
    
    /// Fecthing API
    func getForecast(forLatitude lat: Double, Longitude long: Double, TempratureUnit unit: String) -> Void {
        let urlPath = ApiUrl.BASE_URL + methodName!
        let paramString = "?lat=\(lat)&lon=\(long)&appid=\(ApiUrl.APIKey)&units=\(unit)"
        let request = Request(urlPath + paramString)
        request.execute { (response, error) in
            self.callback?(response)
        }
    }
}
